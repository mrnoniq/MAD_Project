package com.example;

import de.greenrobot.daogenerator.DaoGenerator;
import de.greenrobot.daogenerator.Entity;
import de.greenrobot.daogenerator.Property;
import de.greenrobot.daogenerator.Schema;

public class Generator {

    private static final int SCHEMA_VERSION = 1;
    private String path = "../app/src/main/java";

    private Schema schema = new Schema(SCHEMA_VERSION, "com.example.andi.omdbapp.dbmodel");

    private void generateSchema() throws Exception {
        // entity for movie data
        Entity savedMovie = schema.addEntity("SavedMovie");
        savedMovie.addStringProperty("imdbID").notNull().primaryKey();
        savedMovie.addStringProperty("title").notNull();
        savedMovie.addStringProperty("year").notNull();

        // entity for favourites
        Entity favourite = schema.addEntity("Favourite");
        Property favouriteImdbID = favourite.addStringProperty("imdbID").primaryKey().getProperty();
        favourite.addToOne(savedMovie, favouriteImdbID);

        // entity for watchlist
        Entity toWatch = schema.addEntity("ToWatch");
        Property toWatchImdbID = toWatch.addStringProperty("imdbID").primaryKey().getProperty();
        toWatch.addToOne(savedMovie, toWatchImdbID);

        new DaoGenerator().generateAll(schema, path);
    }

    public static void main(String args[]) {
        try {
            new Generator().generateSchema();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
