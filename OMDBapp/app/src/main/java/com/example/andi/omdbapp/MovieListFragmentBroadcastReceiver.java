package com.example.andi.omdbapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import java.util.ArrayList;

public class MovieListFragmentBroadcastReceiver extends BroadcastReceiver {

    // actions
    public static final String ACTION_SHOW_LIST = "com.example.andi.omdbapp.action.SHOW_LIST";

    // extras
    public static final String EXTRA_MOVIE_LIST = "com.example.andi.omdbapp.extra.MOVIE_LIST";

    MovieListFragment movieListFragment;

    public MovieListFragmentBroadcastReceiver(MovieListFragment movieListFragment) {
        this.movieListFragment = movieListFragment;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SHOW_LIST.equals(action)) {
                // update movies in list
                ArrayList<Movie> movies = intent.getParcelableArrayListExtra(EXTRA_MOVIE_LIST);
                movieListFragment.updateMovies(movies);
            }
        }
    }
}