package com.example.andi.omdbapp;

import android.content.Intent;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

public class DetailsFragment extends Fragment {

    private static final String ARG_MOVIE = "com.example.andi.omdbapp.arg.MOVIE";

    private Movie movie;

    public DetailsFragment() {
        // Required empty public constructor
    }

    public static DetailsFragment newInstance(Movie movie) {
        DetailsFragment fragment = new DetailsFragment();
        Bundle args = new Bundle();
        args.putParcelable(ARG_MOVIE, movie);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            movie = getArguments().getParcelable(ARG_MOVIE);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_details, container, false);

        //set content of textviews
        if (movie != null) {
            ((TextView) (view.findViewById(R.id.tvTitle))).setText(movie.getTitle());
            ((TextView) (view.findViewById(R.id.tvYear))).setText(movie.getYear());
            ((TextView) (view.findViewById(R.id.tvRated))).setText(movie.getRated());
            ((TextView) (view.findViewById(R.id.tvReleased))).setText(movie.getReleased());
            ((TextView) (view.findViewById(R.id.tvRuntime))).setText(movie.getRuntime());
            ((TextView) (view.findViewById(R.id.tvGenre))).setText(movie.getGenre());
            ((TextView) (view.findViewById(R.id.tvDirector))).setText(movie.getDirector());
            ((TextView) (view.findViewById(R.id.tvWriter))).setText(movie.getWriter());
            ((TextView) (view.findViewById(R.id.tvActors))).setText(movie.getActors());
            ((TextView) (view.findViewById(R.id.tvPlot))).setText(movie.getPlot());
        }

        Button btnAddWatchlist = (Button) view.findViewById(R.id.btnAddWatchlist);
        Button btnRemoveWatchlist = (Button) view.findViewById(R.id.btnRemoveWatchlist);
        Button btnAddFavourites = (Button) view.findViewById(R.id.btnAddFavourites);
        Button btnRemoveFavourites = (Button) view.findViewById(R.id.btnRemoveFavourites);

        btnAddWatchlist.setOnClickListener(onClickListener);
        btnRemoveWatchlist.setOnClickListener(onClickListener);
        btnAddFavourites.setOnClickListener(onClickListener);
        btnRemoveFavourites.setOnClickListener(onClickListener);

        return view;
    }

    // listener for adding/removing to watchlist/favourites, sends broadcast to activity
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = new Intent();
            switch (v.getId()) {
                case R.id.btnAddWatchlist:
                    intent.setAction(MainActivityBroadcastReceiver.ACTION_ADD_WATCHLIST);
                    break;
                case R.id.btnRemoveWatchlist:
                    intent.setAction(MainActivityBroadcastReceiver.ACTION_REMOVE_WATCHLIST);
                    break;
                case R.id.btnAddFavourites:
                    intent.setAction(MainActivityBroadcastReceiver.ACTION_ADD_FAVOURITES);
                    break;
                case R.id.btnRemoveFavourites:
                    intent.setAction(MainActivityBroadcastReceiver.ACTION_REMOVE_FAVOURITES);
                    break;
            }
            intent.putExtra(MainActivityBroadcastReceiver.EXTRA_MOVIE, movie);
            LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
        }
    };
}
