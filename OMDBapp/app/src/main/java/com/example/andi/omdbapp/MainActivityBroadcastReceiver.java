package com.example.andi.omdbapp;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class MainActivityBroadcastReceiver extends BroadcastReceiver {

    // actions
    public static final String ACTION_LOAD_DETAILS = "com.example.andi.omdbapp.action.LOAD_DETAILS";
    public static final String ACTION_ADD_FAVOURITES = "com.example.andi.omdbapp.action.ADD_FAVOURITES";
    public static final String ACTION_REMOVE_FAVOURITES = "com.example.andi.omdbapp.action.REMOVE_FAVOURITES";
    public static final String ACTION_ADD_WATCHLIST = "com.example.andi.omdbapp.action.ADD_WATCHLIST";
    public static final String ACTION_REMOVE_WATCHLIST = "com.example.andi.omdbapp.action.REMOVE_WATCHLIST";

    // extras
    public static final String EXTRA_IMDBID = "com.example.andi.omdbapp.extra.IMDBID";
    public static final String EXTRA_MOVIE = "com.example.andi.omdbapp.extra.MOVIE";

    private MainActivity mainActivity;

    public MainActivityBroadcastReceiver(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_LOAD_DETAILS.equals(action)) {
                // load details
                final String imdbID = intent.getStringExtra(EXTRA_IMDBID);
                mainActivity.loadDetails(imdbID);
            } else if (ACTION_ADD_FAVOURITES.equals(action)) {
                // add movie to favourites
                Movie movie = intent.getParcelableExtra(EXTRA_MOVIE);
                mainActivity.addToFavourites(movie);
            } else if (ACTION_REMOVE_FAVOURITES.equals(action)) {
                // remove movie from favourites
                Movie movie = intent.getParcelableExtra(EXTRA_MOVIE);
                mainActivity.removeFromFavourites(movie);
            } else if (ACTION_ADD_WATCHLIST.equals(action)) {
                // add movie to watchlist
                Movie movie = intent.getParcelableExtra(EXTRA_MOVIE);
                mainActivity.addToWatchlist(movie);
            } else if (ACTION_REMOVE_WATCHLIST.equals(action)) {
                // remove movie from watchlist
                Movie movie = intent.getParcelableExtra(EXTRA_MOVIE);
                mainActivity.removeFromWatchlist(movie);
            }
        }
    }
}