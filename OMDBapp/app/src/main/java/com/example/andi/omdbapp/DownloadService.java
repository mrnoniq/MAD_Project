package com.example.andi.omdbapp;

import android.app.IntentService;
import android.content.Intent;
import android.content.Context;
import android.os.Bundle;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.RequestFuture;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.TimeoutException;

public class DownloadService extends IntentService {
    // Actions
    private static final String ACTION_SEARCH = "com.example.andi.omdbapp.action.SEARCH";
    private static final String ACTION_LOAD_DETAILS = "com.example.andi.omdbapp.action.LOAD_DETAILS";

    // Parameters
    private static final String EXTRA_TITLE = "com.example.andi.omdbapp.extra.TITLE";
    private static final String EXTRA_IMDBID = "com.example.andi.omdbapp.extra.IMDBID";
    private static final String EXTRA_MESSENGER = "com.example.andi.omdbapp.extra.MESSENGER";

    // OMDb URLs
    private static final String OMDB_SEARCH_URL = "http://omdbapi.com/?r=json&s=";
    private static final String OMDB_DETAILS_URL = "http://omdbapi.com/?r=json&i=";

    // OMDb Parameters
    private static final String OMDB_SUCCESS = "Response";
    private static final String OMDB_ID = "imdbID";
    private static final String OMDB_TITLE = "Title";
    private static final String OMDB_YEAR = "Year";
    private static final String OMDB_RATED = "Rated";
    private static final String OMDB_RELEASED = "Released";
    private static final String OMDB_RUNTIME = "Runtime";
    private static final String OMDB_GENRE = "Genre";
    private static final String OMDB_DIRECTOR = "Director";
    private static final String OMDB_WRITER = "Writer";
    private static final String OMDB_ACTORS = "Actors";
    private static final String OMDB_PLOT = "Plot";
    private static final String OMDB_SEARCH_RESULT = "Search";

    // Message Data
    public static final String MESSAGE_SEARCH_RESULT = "com.example.andi.omdbapp.message.SEARCH_RESULT";
    public static final String MESSAGE_DETAILS = "com.example.andi.omdbapp.message.DETAILS";

    public DownloadService() {
        super("DownloadService");
    }

    public static void startActionSearch(Context context, String title, Messenger messenger) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_SEARCH);
        intent.putExtra(EXTRA_TITLE, title);
        intent.putExtra(EXTRA_MESSENGER, messenger);
        context.startService(intent);
    }

    public static void startActionLoadDetails(Context context, String imdbID, Messenger messenger) {
        Intent intent = new Intent(context, DownloadService.class);
        intent.setAction(ACTION_LOAD_DETAILS);
        intent.putExtra(EXTRA_IMDBID, imdbID);
        intent.putExtra(EXTRA_MESSENGER, messenger);
        context.startService(intent);
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
            final String action = intent.getAction();
            if (ACTION_SEARCH.equals(action)) {
                final String title = intent.getStringExtra(EXTRA_TITLE);
                Messenger messenger = intent.getParcelableExtra(EXTRA_MESSENGER);
                handleActionSearch(title, messenger);
            } else if (ACTION_LOAD_DETAILS.equals(action)) {
                final String imdbID = intent.getStringExtra(EXTRA_IMDBID);
                Messenger messenger = intent.getParcelableExtra(EXTRA_MESSENGER);
                handleActionLoadDetails(imdbID, messenger);
            }
        }
    }

    private void handleActionSearch(String title, Messenger messenger) {
        title = URLEncoder.encode(title);
        String url = OMDB_SEARCH_URL + title;

        // setup volley request
        RequestQueue queue = Volley.newRequestQueue(this);
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(url, null, future, future);
        queue.add(request);

        try {
            // wait for response
            JSONObject response = future.get(10, TimeUnit.SECONDS);
            if (response.getString(OMDB_SUCCESS).equals("True")) {
                // send message with search result to handler
                Message msg = Message.obtain();
                Bundle data = new Bundle();
                data.putParcelableArrayList(MESSAGE_SEARCH_RESULT, parseSearchResponse(response));
                msg.setData(data);
                messenger.send(msg);
            }
        } catch (InterruptedException | ExecutionException | JSONException | TimeoutException | RemoteException e) {
            e.printStackTrace();
        }
    }

    private ArrayList<Movie> parseSearchResponse(JSONObject response) throws JSONException {
        JSONArray moviesJSON = response.getJSONArray(OMDB_SEARCH_RESULT);
        ArrayList<Movie> movies = new ArrayList<>();

        for (int i = 0; i < moviesJSON.length(); i++) {
            JSONObject movieJSON = moviesJSON.getJSONObject(i);
            String imdbID = movieJSON.getString(OMDB_ID);
            String title = movieJSON.getString(OMDB_TITLE);
            String year = movieJSON.getString(OMDB_YEAR);
            movies.add(new Movie(imdbID, title, year));
        }

        return movies;
    }

    private void handleActionLoadDetails(String imdbID, Messenger messenger) {
        String url = OMDB_DETAILS_URL + imdbID;

        RequestQueue queue = Volley.newRequestQueue(this);
        RequestFuture<JSONObject> future = RequestFuture.newFuture();
        JsonObjectRequest request = new JsonObjectRequest(url, null, future, future);
        queue.add(request);

        try {
            JSONObject response = future.get(10, TimeUnit.SECONDS);
            if (response.getString(OMDB_SUCCESS).equals("True")) {
                Message msg = Message.obtain();
                Bundle data = new Bundle();
                data.putParcelable(MESSAGE_DETAILS, parseDetailsResponse(response));
                msg.setData(data);
                messenger.send(msg);
            }
        } catch (InterruptedException | ExecutionException | JSONException | TimeoutException | RemoteException e) {
            e.printStackTrace();
        }
    }

    private Movie parseDetailsResponse(JSONObject response) throws JSONException {
        String imdbID = response.getString(OMDB_ID);
        String title = response.getString(OMDB_TITLE);
        String year = response.getString(OMDB_YEAR);
        String rated = response.getString(OMDB_RATED);
        String released = response.getString(OMDB_RELEASED);
        String runtime = response.getString(OMDB_RUNTIME);
        String genre = response.getString(OMDB_GENRE);
        String director = response.getString(OMDB_DIRECTOR);
        String writer = response.getString(OMDB_WRITER);
        String actors = response.getString(OMDB_ACTORS);
        String plot = response.getString(OMDB_PLOT);

        return new Movie(imdbID, title, year, rated, released, runtime, genre, director, writer, actors, plot);
    }
}
