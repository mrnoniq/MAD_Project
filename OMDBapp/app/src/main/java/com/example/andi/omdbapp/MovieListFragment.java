package com.example.andi.omdbapp;

import android.app.ListFragment;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v4.content.LocalBroadcastManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

public class MovieListFragment extends ListFragment {

    private MovieArrayAdapter listAdapter;

    ArrayList<Movie> movies = new ArrayList<>();

    private MovieListFragmentBroadcastReceiver broadcastReceiver;

    public MovieListFragment() {
        broadcastReceiver = new MovieListFragmentBroadcastReceiver(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_movie_list, container, false);

        listAdapter = new MovieArrayAdapter(getContext(), 0, movies);
        setListAdapter(listAdapter);

        return view;
    }

    @Override
    public void onListItemClick(ListView listView, View view, int position, long id) {
        Movie movie = (Movie) getListAdapter().getItem(position);
        // send broadcast with id and action for loading details
        Intent intent = new Intent(MainActivityBroadcastReceiver.ACTION_LOAD_DETAILS);
        intent.putExtra(MainActivityBroadcastReceiver.EXTRA_IMDBID, movie.getImdbID());
        LocalBroadcastManager.getInstance(getContext()).sendBroadcast(intent);
    }

    @Override
    public void onResume() {
        super.onResume();
        // register broadcast receiver
        LocalBroadcastManager.getInstance(getContext()).registerReceiver(broadcastReceiver, new IntentFilter(MovieListFragmentBroadcastReceiver.ACTION_SHOW_LIST));
    }

    @Override
    public void onPause() {
        super.onPause();
        LocalBroadcastManager.getInstance(getContext()).unregisterReceiver(broadcastReceiver);
    }

    public void updateMovies(ArrayList<Movie> movies) {
        this.movies = movies;
        if (listAdapter != null) {
            // update adapter
            listAdapter.clear();
            listAdapter.addAll(this.movies);
            listAdapter.notifyDataSetChanged();
        }
    }

}
