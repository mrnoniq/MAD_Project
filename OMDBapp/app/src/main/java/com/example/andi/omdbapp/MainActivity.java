package com.example.andi.omdbapp;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Messenger;

import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import com.example.andi.omdbapp.dbmodel.DaoMaster;
import com.example.andi.omdbapp.dbmodel.DaoSession;
import com.example.andi.omdbapp.dbmodel.Favourite;
import com.example.andi.omdbapp.dbmodel.FavouriteDao;
import com.example.andi.omdbapp.dbmodel.SavedMovie;
import com.example.andi.omdbapp.dbmodel.SavedMovieDao;
import com.example.andi.omdbapp.dbmodel.ToWatch;
import com.example.andi.omdbapp.dbmodel.ToWatchDao;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.dao.query.QueryBuilder;

public class MainActivity extends AppCompatActivity {

    //fragment tags
    private static final String FRAGMENT_TAG_LIST = "com.example.andi.omdbapp.fragment.tag.LIST";
    private static final String FRAGMENT_TAG_DETAILS = "com.example.andi.omdbapp.fragment.tag.DETAILS";

    // database name
    private static final String DATABASE_NAME = "omdbapp-db";

    // messengers for handlers
    private Messenger detailsMessenger;
    private Messenger searchMessenger;

    private MainActivityBroadcastReceiver broadcastReceiver;

    // database objects
    private SQLiteDatabase databaseConnection;
    private DaoMaster daoMaster;
    private DaoSession daoSession;

    private SavedMovieDao savedMovieDao;
    private FavouriteDao favouriteDao;
    private ToWatchDao toWatchDao;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        detailsMessenger = new Messenger(new DetailsHandler());
        searchMessenger = new Messenger(new SearchHandler());
        broadcastReceiver = new MainActivityBroadcastReceiver(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        // register broadcast receiver
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(MainActivityBroadcastReceiver.ACTION_LOAD_DETAILS);
        intentFilter.addAction(MainActivityBroadcastReceiver.ACTION_ADD_FAVOURITES);
        intentFilter.addAction(MainActivityBroadcastReceiver.ACTION_REMOVE_FAVOURITES);
        intentFilter.addAction(MainActivityBroadcastReceiver.ACTION_ADD_WATCHLIST);
        intentFilter.addAction(MainActivityBroadcastReceiver.ACTION_REMOVE_WATCHLIST);
        LocalBroadcastManager.getInstance(this).registerReceiver(broadcastReceiver, intentFilter);

        // init db connection
        initDatabaseConnection();
    }

    @Override
    protected void onPause() {
        super.onPause();
        // unregister broadcast receiver
        LocalBroadcastManager.getInstance(this).unregisterReceiver(broadcastReceiver);

        // close db connection
        closeDatabaseConnection();
    }

    private class DetailsHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            Movie movie = msg.getData().getParcelable(DownloadService.MESSAGE_DETAILS);
            showDetails(movie);
        }
    }

    private class SearchHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            ArrayList<Movie> movies = msg.getData().getParcelableArrayList(DownloadService.MESSAGE_SEARCH_RESULT);

            // send broadcast with search results and action for showing list
            Intent intent = new Intent(MovieListFragmentBroadcastReceiver.ACTION_SHOW_LIST);
            intent.putParcelableArrayListExtra(MovieListFragmentBroadcastReceiver.EXTRA_MOVIE_LIST, movies);
            LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
        }
    }

    public void showList(ArrayList<Movie> movies) {
        FragmentManager fragmentManager = getFragmentManager();

        // find existing fragment or create new
        MovieListFragment list = (MovieListFragment) fragmentManager.findFragmentByTag(FRAGMENT_TAG_LIST);
        if (list == null) {
            list = new MovieListFragment();
        }

        // add fragment if not added
        if (!list.isAdded()) {
            list.updateMovies(movies);
            FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.replace(R.id.fragmentContainer, list, FRAGMENT_TAG_LIST);
            fragmentTransaction.commit();
        }
    }

    public void loadDetails(String imdbID) {
        //start service for loading details
        DownloadService.startActionLoadDetails(this, imdbID, detailsMessenger);
    }

    public void showDetails(Movie movie) {
        FragmentManager fragmentManager = getFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        DetailsFragment details = DetailsFragment.newInstance(movie);
        fragmentTransaction.replace(R.id.fragmentContainer, details, FRAGMENT_TAG_DETAILS);
        fragmentTransaction.commit();
    }

    public void onSearchClick(View view) {
        // show list fragment
        showList(new ArrayList<Movie>());

        // start service for loading search results
        String title = ((EditText) findViewById(R.id.editTextSearch)).getText().toString();
        DownloadService.startActionSearch(this, title, searchMessenger);

        // hide keyboard
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) { }
    }

    public void onFavouritesClick(View view) {
        // load favourites
        ArrayList<Movie> movies = queryFavourites();

        // show list fragment
        showList(movies);

        // send broadcast to update list
        Intent intent = new Intent(MovieListFragmentBroadcastReceiver.ACTION_SHOW_LIST);
        intent.putParcelableArrayListExtra(MovieListFragmentBroadcastReceiver.EXTRA_MOVIE_LIST, movies);
        LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
    }

    public void onWatchlistClick(View view) {
        // load watchlist
        ArrayList<Movie> movies = queryToWatch();

        // show list fragment
        showList(movies);

        // send broadcast to update list
        Intent intent = new Intent(MovieListFragmentBroadcastReceiver.ACTION_SHOW_LIST);
        intent.putParcelableArrayListExtra(MovieListFragmentBroadcastReceiver.EXTRA_MOVIE_LIST, movies);
        LocalBroadcastManager.getInstance(MainActivity.this).sendBroadcast(intent);
    }

    // init db connection
    private void initDatabaseConnection() {
        databaseConnection = new DaoMaster.DevOpenHelper(this, DATABASE_NAME, null).getWritableDatabase();
        daoMaster = new DaoMaster(databaseConnection);
        daoSession = daoMaster.newSession();
        savedMovieDao = daoSession.getSavedMovieDao();
        favouriteDao = daoSession.getFavouriteDao();
        toWatchDao = daoSession.getToWatchDao();
    }

    // close db connection
    private void closeDatabaseConnection() {
        if (databaseConnection != null && databaseConnection.isOpen()) {
            databaseConnection.close();
        }
    }

    // get list of favourites
    private ArrayList<Movie> queryFavourites() {
        QueryBuilder<SavedMovie> builder = savedMovieDao.queryBuilder();
        builder.join(Favourite.class, FavouriteDao.Properties.ImdbID);
        List<SavedMovie> movies = builder.list();
        return savedMovieListToMovieList(movies);
    }

    // get watchlist
    private ArrayList<Movie> queryToWatch() {
        QueryBuilder<SavedMovie> builder = savedMovieDao.queryBuilder();
        builder.join(ToWatch.class, ToWatchDao.Properties.ImdbID);
        List<SavedMovie> movies = builder.list();
        return savedMovieListToMovieList(movies);
    }

    // convert query results to movie list
    private ArrayList<Movie> savedMovieListToMovieList(List<SavedMovie> savedMovies) {
        ArrayList<Movie> movies = new ArrayList<>();
        for (int i = 0; i < savedMovies.size(); i++) {
            SavedMovie tmp = savedMovies.get(i);
            movies.add(new Movie(tmp.getImdbID(), tmp.getTitle(), tmp.getYear()));
        }
        return movies;
    }

    // add movie to favourites
    public void addToFavourites(Movie movie) {
        // insert movie if not in db
        SavedMovie savedMovie = savedMovieDao.load(movie.getImdbID());
        if (savedMovie == null) {
            savedMovie = new SavedMovie(movie.getImdbID(), movie.getTitle(), movie.getYear());
            savedMovieDao.insert(savedMovie);
        }
        // insert entry for favourites
        Favourite favourite = new Favourite();
        favourite.setSavedMovie(savedMovie);
        favouriteDao.insertOrReplace(favourite);
    }

    // add movie to watchlist
    public void addToWatchlist(Movie movie) {
        // insert movie if not in db
        SavedMovie savedMovie = savedMovieDao.load(movie.getImdbID());
        if (savedMovie == null) {
            savedMovie = new SavedMovie(movie.getImdbID(), movie.getTitle(), movie.getYear());
            savedMovieDao.insert(savedMovie);
        }
        // insert entry for watchlist
        ToWatch toWatch = new ToWatch();
        toWatch.setSavedMovie(savedMovie);
        toWatchDao.insertOrReplace(toWatch);
    }

    // remove movie from favourites
    public void removeFromFavourites(Movie movie) {
        // delete entry from favourites
        favouriteDao.deleteByKey(movie.getImdbID());
        // delete entire movie if no longer needed
        if (toWatchDao.load(movie.getImdbID()) == null) {
            savedMovieDao.deleteByKey(movie.getImdbID());
        }
    }

    // remove movie from watchlist
    public void removeFromWatchlist(Movie movie) {
        // delete entry from watchlist
        toWatchDao.deleteByKey(movie.getImdbID());
        // delete entire movie if no longer needed
        if (favouriteDao.load(movie.getImdbID()) == null) {
            savedMovieDao.deleteByKey(movie.getImdbID());
        }
    }

}
